module PersonalSite
    # Takes an url as an input and appends the current time to it
    def timestamp(input_url)
      "#{input_url}?#{Time.now.to_i}"
    end
  
    #takes a repo name as an input and finds the according logo
    def find_logo(input)
      unknown_logo = '/img/logos/unknown.png'
      unknown_logo unless input.is_a? String
      config = @context.registers[:site].config
  
      wanted_file = '/img/logos/' + input.gsub("'", '').gsub(' ', '').gsub('!', '') + '.png'
  
      File.exist?(config['source'] + wanted_file) ? wanted_file : unknown_logo
    end
  end
  
  Liquid::Template.register_filter(PersonalSite)
  